package com.coomeva.consulta.diccionario;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;


@SpringBootApplication
public class ConsultaDiccionarioDatosApplication {

	public static void main(String[] args) throws IOException {
		SpringApplication.run(ConsultaDiccionarioDatosApplication.class, args);
		ArrayList<String> campos = new ArrayList<>();
		try {
			InputStream ins = new FileInputStream(ConsultaDiccionarioDatosApplication.class.getClassLoader().getResource("campos.txt").getPath());
			Scanner obj = new Scanner(ins);
			while (obj.hasNextLine())campos.add(obj.nextLine());

			}catch(Exception e){

			}
				
		File file2 = new File(ConsultaDiccionarioDatosApplication.class.getClassLoader().getResource("response.txt").getPath());
				
		 if (!file2.exists()) {
             file2.createNewFile();
         }else {
        	 file2.delete();
         }
			
		FileWriter fw = new FileWriter(file2,true);
		BufferedWriter bw = new BufferedWriter(fw);
				
		for (String campo : campos) {
			
			if(campo.length()>6) {
				System.out.println(campo);
				bw.write("CAMPO:" + campo+ " EXCEDE EL LIMITE DE CARACTERES ACEPTADOS POR EL SERVICIO");

			}else {
				String url ="http://181.57.139.181:10024/web/services/WSPTE00012";
				
				ArrayList<String> results = new ArrayList<>();
				
				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.APPLICATION_JSON);

				String JSON_DATA =	"{" 
					    		   + "      \"I_CODEMP\": \"67890\"," 
					    		   + "      \"I_CODAMB\": \"MUl\","                  
					    		   + "      \"I_INDLEN\" : \"1\"," 
					    		   + "      \"I_CANAL\" : \"2\"," 
					    		   + "      \"I_USRCAN\" : \"MULSERVICE\"," 
					    		   + "      \"I_NOMCMP\" : ";
				
				String FIELD_DATA= "\""+campo+"\"}"; 
				
				String REQUEST = JSON_DATA+FIELD_DATA;
				
				
				HttpEntity<String> entity = new HttpEntity<String>(REQUEST, headers);
				ResponseEntity<String> response = getRestTemplate().postForEntity(url, entity, String.class);
							
				String responseField = response.toString().substring(response.toString().indexOf("\"O_CONT\""));
				
				String codRetorno = (responseField.substring(responseField.indexOf(":")+1, responseField.indexOf(":")+2) );
				System.out.println(campo);

					
				bw.write(((Integer.parseInt(codRetorno)==1)?"CAMPO:" + campo+ " SE ENCONTRÓ CORRECTAMENTE, MENSAJE RETORNO: ":"CAMPO:" + campo+ " NO SE ENCONTRÓ, MENSAJE RETORNO: ") + responseField);
				bw.write("\r\n");
			}
			
		}
		bw.close();
	}
	

	@Bean
	public static RestTemplate getRestTemplate() {
		return new RestTemplate();		
	}


}
